Integrantes:
Andres Felipe Jimenez Albornoz,
Mateo Montoya Pachon




# Codigos #
#Codigo de cliente#
//Import de librerias
from udp import *
from time import *

//Imprimir por consola los datos que entran
def onUDPReceive(ip, port, data):
	print(data);

def main():
	//crea una variable con la libreria de udp socket
	socket = UDPSocket()
	//recibe los datos por parte del servidor
	socket.onReceive(onUDPReceive)
	//inicia la comunicacion con el servidor
	socket.begin(1235)
	
	
	
	count = 0	
	while count < 10:
		count += 1
		//Envia los 10 pings al servidor
		socket.send("192.168.0.3", 1235, "Ping_" + str(count))
		//Espera un segundo entre los paquetes enviados
		sleep(1)
		

if __name__ == "__main__":
	main()
	

#Codigo de servidor#
from udp import *
from time import *
import random

socket = UDPSocket()


def onUDPReceive(ip, port, data):
	tiempo = uptime()
	//crea un numero random entre 0 y 10
	rand = random.randint(0,10)
	//pone en mayusculas los datos entrantes
	datos = data.upper();
	//envia el dato devuelta si el numero aleatorio es menor que 4
	if rand < 4:
		socket.send("192.168.0.2", 1235, "Return:  "+ datos
					+ " Time: "+str(uptime()))
	
	else:
		socket.send("192.168.0.2", 1235, "Request time out")

def main():
	//escucha continuamente
	while True:
		socket.begin(1235)
		socket.onReceive(onUDPReceive)
	


if __name__ == "__main__":
	main()


#Explicacion de los codigos#
Primero se iniciaba el UDP server socket el cual mantenía escuchando los datos entrantes. Después se procedió a iniciar el UDP client socket el cual empezaba a enviar 10 pings al servidor con el formato Ping_(número de ping). El cliente tenía la dirección IP 192.168.0.2 y el servidor tenía dirección IP 192.168.0.3. Ambos con el puerto 1235. El servidor recibía cada ping que enviaba el cliente y a partir de un numero aleatorio que se generaba por solicitud del cliente, se enviaba el mismo formato con el cual llego el mensaje, pero en mayúsculas. Si el numero era menor a 4 se enviaba el mensaje, pero si era mayor, enviaba un mensaje de “Request time out” simulando que el dato se perdió. El proceso se repetía por la cantidad de pings que envió el cliente (10) y el programa del client socket se detenía.




#Video#

https://youtu.be/xAxOPOb1vQU



#Resultados#

Para el proyecto de redes, tuvimos unos resultados muy buenos logramos llevar a cabo casi que todo el material que se nos requirio, para este proyecto logramos hacer que en la conexion pc servidor el pc lograra enviar 10 pings hacia el servidor, el cual
los recibia y determinaba gracia a la implementacion de un numero aleatorio generado en el codigo. Si este devolveria los pings al pc o enviaria el mensaje "Request time out " en el cual si el numero generado aleatoriamente era mayor que 4 se enviaria el mensaje 
yu esi el numero era menor que 4 el servidor devolveria los pings al pc. Sin embargo, encontramos un problema el cual no pudimos resolver. No pudimos hacer que pasado un segundo de haber enviado los pings se parara la operacion y se 
enviara un mensaje de "Request time out" esto fue debido a que en python, el lenguaje que utilizamos, el api que usa packet tracer de python no tenia la funcionalidad de settimeout por lo cual no pudimos seguir con la implementacion de este paso.

Se encontró una publicación en stackoverflow en la cual mostraba esta misma limitación, por lo cual en los comentarios sugerían otras aplicaciones como GNS3 para este tipo de simulación de sockets.



#Referencias#

PYMOTW. User Datagram Client and Server - Python Module of the Week. (n.d.). Retrieved November 13, 2021, from https://pymotw.com/2/socket/udp.html. 

UDP client/server socket in Python. Stack Overflow. Retrieved November 13, 2021, from https://stackoverflow.com/questions/27893804/udp-client-server-socket-in-python. 



